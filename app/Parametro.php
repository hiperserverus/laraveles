<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Parametro extends Model
{

    protected $table="BA.TB_MS_PARAMETRO";  
    protected $primaryKey = 'c_parametro_id';
    protected $keyType = 'string';
    protected $fillable = ['c_parametro_id', 'c_grupo_parametro_id', 'c_codigo_parametro', 'c_desc_parametro', 'c_parametro_activo'];

    //protected $dateFormat = 'U';

    public $incrementing = false;    
    public $timestamps = false;

    public function grupoparametro()
    {
        return $this->belongsTo('App\GrupoParametro', 'c_grupo_parametro_id', 'c_grupo_parametro_id');
        //Un parametro pertenece a  un solo grupoparametro
    }

}
