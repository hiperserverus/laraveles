<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    protected $table="SEGURIDAD.TB_SG_USUARIO";    
    protected $primaryKey = 'c_usuario_id';
    protected $keyType = 'string';
	protected $fillable = [
		'c_usuario_id', 'c_tipo_usuario_id', 'c_perfil_id', 'c_codigo_usuario', 'c_nombre_usuario', 'c_apellido_usuario',
		'c_clave_usuario', 'f_asignacion_clave', 'c_usuario_bloqueado', 'c_cod_usuario', 'c_usuario_activo', 'c_auditoria_id'
	];

    //protected $dateFormat = 'U';

    public $incrementing = false;   
    public $timestamps = false;

    public function tipousuario()
    {       
        return $this->hasOne('App\TipoUsuario', 'c_parametro_id', 'c_tipo_usuario_id');
    }


    public function perfil()
    {    	
		return $this->belongsTo('App\Perfil', 'c_perfil_id', 'c_perfil_id');
    }

    public function roles()
    {
        //return $this->belongsToMany('App\Objeto', 'xxxx', 'xxxxx')->using('App\RolObjeto');
        return $this->belongsToMany('App\Rol', 'SEGURIDAD.TB_SG_USUARIO_ROL', 'c_usuario_id', 'c_rol_id')->using('App\UsuarioRol');
    } 

    public function objetos()
    {    
        return $this->belongsToMany('App\Objeto', 'SEGURIDAD.TB_SG_USUARIO_OBJETO', 'c_usuario_id', 'c_objeto_id')->using('App\UsuarioObjeto');
    }

    public function unidadesorganicas()
    {
        return $this->belongsToMany('App\Unidad', 'SEGURIDAD.TB_SG_UNIDAD_USUARIO', 'c_usuario_id', 'c_unidad_id')->using('App\UnidadUsuario');
    } 

    public function empresas()
    {
        //return $this->belongsToMany('App\Objeto', 'xxxx', 'xxxxx')->using('App\RolObjeto');
        return $this->belongsToMany('App\Empresa', 'SEGURIDAD.TB_SG_UNIDAD_USUARIO', 'c_usuario_id', 'c_unidad_id')->using('App\UnidadUsuario');
    } 

    public function unidadesnegocios()
    {
        return $this->belongsToMany('App\UnidadNegocio', 'SEGURIDAD.TB_SG_UNIDAD_USUARIO', 'c_usuario_id', 'c_unidad_id')->using('App\UnidadUsuario');
    } 

    public function gruposempresariales()
    {    
        return $this->belongsToMany('App\GrupoEmpresarial', 'SEGURIDAD.TB_SG_UNIDAD_USUARIO', 'c_usuario_id', 'c_unidad_id')->using('App\UnidadUsuario');
    }     
  
}
