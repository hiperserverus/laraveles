<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Catalogo extends Model
{
    protected $table="BA.TB_MS_CATALOGO";    
    protected $primaryKey = 'c_cata_id';
    protected $keyType = 'string';
	protected $fillable = [
		'c_cata_id', 'c_codigo_cata', 'c_nombre_cata', 'c_desc_cata', 'c_esquema', 'c_cata_activo'
	];

    //protected $dateFormat = 'U';

    public $incrementing = false;   
    public $timestamps = false;       

    public function series()
    {
        return $this->hasMany('App\CatalogoSerie', 'c_cata_id', 'c_cata_id');
    }

}
