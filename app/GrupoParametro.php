<?php

namespace App;
use App\Parametro;

use Illuminate\Database\Eloquent\Model;

class GrupoParametro extends Model
{
    //const CREATED_AT = 'creation_date';
    //const UPDATED_AT = 'last_update';
	//protected $connection = 'connection-name';

    protected $table="BA.TB_MS_GRUPO_PARAMETRO";
    protected $primaryKey = 'C_GRUPO_PARAMETRO_ID';
    protected $keyType = 'string';

    //protected $dateFormat = 'U';

    protected $fillable = ['C_CODIGO_GRUPO_PARAMETRO', 'C_DES_GRUPO_PARAMETRO', 'C_GRUPO_PARAMETRO_ACTIVO'];

    public $incrementing = false;    
    public $timestamps = false;
    
    public function parametros()
    {
        return $this->hasMany('App\Parametro', 'c_grupo_parametro_id', 'c_grupo_parametro_id');
    }

}


