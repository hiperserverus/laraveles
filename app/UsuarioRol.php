<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class UsuarioRol extends Pivot
{
    protected $table="SEGURIDAD.TB_SG_USUARIO_ROL";
    protected $primaryKey = 'c_usuario_rol_id';
    protected $keyType = 'string';
	protected $fillable = [
		'c_usuario_rol_id', 'c_usuario_id', 'c_rol_id', 'c_usuario_rol_activo', 'c_auditoria_id' 
	];

    //protected $dateFormat = 'U';

    public $incrementing = false;   
    public $timestamps = false;
}
