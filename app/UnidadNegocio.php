<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnidadNegocio extends Model
{
    protected $table="BASE.TB_MG_DIVISION";    
    protected $primaryKey = 'c_division_id';
    protected $keyType = 'string';
	protected $fillable = [
		'c_division_id', 'c_empresa_id', 'c_hblt', 'c_prncpal', 'c_drcc_empr_id', 'c_division_activa', 'c_auditoria_id'
	];

    //protected $dateFormat = 'U';

    public $incrementing = false;   
    public $timestamps = false;    
    
	public function unidadorganica() 
	{
	    return $this->hasOne('App\Unidad', 'c_unidad_id', 'c_division_id');
	}

    public function usuarios()
    {
        return $this->belongsToMany('App\Usuario', 'SEGURIDAD.TB_SG_UNIDAD_USUARIO', 'c_unidad_id', 'c_usuario_id')->using('App\UnidadUsuario');
    } 	
}
