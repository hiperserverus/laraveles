<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ayuda extends Model
{
    protected $table="BA.TB_MS_AYUDA_CBCRA";    
    protected $primaryKey = 'c_ayuda_cabecera_id';
    protected $keyType = 'string';
	protected $fillable = [
		'c_ayuda_cabecera_id', 'c_nomb_ayuda', 'c_tabl_relac', 'c_cond_cnslta', 'c_ttlo_ayuda', 'c_prsnta_ayuda', 'c_atrbto_ayuda', 
		'n_max_filas', 'n_clmna_inicio', 'c_ind_sensitivo', 'c_crtrio_inicio', 'c_ayda_cbcra_activa', 'c_orde_cnslta', 'c_rgtr_unco'
	];

    //protected $dateFormat = 'U';

    public $incrementing = false;   
    public $timestamps = false; 

    public function AyudaDetalle()
    {
        return $this->hasMany('App\AyudaDetalle', 'c_ayuda_cabecera_id', 'c_ayuda_cabecera_id');
    }    
}
