<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Perfil extends Model
{
    protected $table="SEGURIDAD.TB_SG_PERFIL";
    protected $primaryKey = 'c_perfil_id';
    protected $keyType = 'string';
	protected $fillable = [
		'c_perfil_id', 'c_unidad_id', 'c_codigo_perfil', 'c_nombre_perfil', 'c_perfil_heredable', 'c_perfil_activo', 'c_auditoria_id'
	];

    //protected $dateFormat = 'U';

    public $incrementing = false;   
    public $timestamps = false;


	public function usuarios () {
    	return $this->hasMany('App\Usuario', 'c_perfil_id', 'c_perfil_id');
	}

	public function empresa(){
    	return $this->belongsTo('App\Empresa', 'c_empresa_id', 'c_unidad_id');
	}

    public function roles()
    {
        return $this->belongsToMany('App\Rol', 'SEGURIDAD.TB_SG_PERFIL_ROL', 'c_perfil_id', 'c_rol_id')->using('App\PerfilRol');
    }   
}

