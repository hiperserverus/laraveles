<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rol extends Model
{
    protected $table="SEGURIDAD.TB_SG_ROL";    
    protected $primaryKey = 'c_rol_id';
    protected $keyType = 'string';
	protected $fillable = [
		'c_rol_id', 'c_unidad_id', 'c_codigo_rol', 'c_desc_rol', 'c_rol_heredable', 'c_rol_activo', 'c_auditoria_id'
	];

    //protected $dateFormat = 'U';

    public $incrementing = false;   
    public $timestamps = false;

    public function empresa()
    {
        return $this->belongsTo('App\Empresa', 'c_empresa_id', 'c_unidad_id');
    }

    public function objetos()
    {
        return $this->belongsToMany('App\Objeto', 'SEGURIDAD.TB_SG_ROL_OBJETO', 'c_rol_id', 'c_objeto_id')->using('App\RolObjeto');
    }    

    public function usuarios()
    {
        return $this->belongsToMany('App\Usuario', 'SEGURIDAD.TB_SG_USUARIO_ROL', 'c_rol_id', 'c_usuario_id')->using('App\UsuarioRol');
    }       

    public function perfiles()
    {
        return $this->belongsToMany('App\Perfil', 'SEGURIDAD.TB_SG_PERFIL_ROL', 'c_rol_id', 'c_perfil_id')->using('App\PerfilRol');
    }           
}
