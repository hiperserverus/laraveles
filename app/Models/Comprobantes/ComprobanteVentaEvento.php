<?php

namespace App\Models\Comprobantes;

use Illuminate\Database\Eloquent\Model;

class ComprobanteVentaEvento extends Model
{
    protected  $table= "ADMINDB.VT_CPBT_VNTA_ELCT_DTLL";
    protected $primaryKey="CONTRIBUYENTE";
    protected $fillable = ['correlativo', 'contribuyente', 'tipo_comprobante', 'serie_correlativo', 'fecha_evento',
                            'tipo_evento', 'estado_evento', 'agente_fecha_envio', 'agente_fecha_confirma', 'agente_respuesta',
                            'portal_fecha_envio', 'portal_fecha_confirma', 'portal_respuesta', 'sunat_fecha_envio', 'sunat_fecha_confirma',
                            'sunat_respuesta', 'yaml_file', 'xml_file', 'zip_file'
                        ];
}
