<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class UsuarioObjeto extends Pivot
{
    protected $table="SEGURIDAD.TB_SG_USUARIO_OBJETO";
    protected $primaryKey = 'c_usuario_objeto_id';
    protected $keyType = 'string';
	protected $fillable = [
		'c_usuario_objeto_id', 'c_usuario_id', 'c_objeto_id', 'c_ind_visible', 'c_ind_habilitado', 'c_ind_protegido', 
		'c_usrio_objto_activo', 'c_auditoria_id', 'c_usuario_objeto_activo'
	];

    //protected $dateFormat = 'U';

    public $incrementing = false;   
    public $timestamps = false;
}
