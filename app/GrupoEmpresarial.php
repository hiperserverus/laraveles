<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GrupoEmpresarial extends Model
{
    protected $table="BASE.TB_MG_HOLDING";    
    protected $primaryKey = 'c_holding_id';
    protected $keyType = 'string';
	protected $fillable = [
		'c_holding_id', 'c_hblt'
	];

    //protected $dateFormat = 'U';

    public $incrementing = false;   
    public $timestamps = false;    
    
	public function unidadorganica() 
	{
	    return $this->hasOne('App\Unidad', 'c_unidad_id', 'c_holding_id');
	}

    public function usuarios()
    {
        return $this->belongsToMany('App\Usuario', 'SEGURIDAD.TB_SG_UNIDAD_USUARIO', 'c_unidad_id', 'c_usuario_id')->using('App\UnidadUsuario');
    } 
}
