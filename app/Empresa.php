<?php

namespace App;

use App\Usuario;
use App\Unidad;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    //
    protected $table="BASE.TB_MG_EMPRESA";    
    protected $primaryKey = 'c_empresa_id';
    protected $keyType = 'string';
	protected $fillable = [
		'c_empresa_id', 'n_ruc', 'c_nomb_cmrcl', 'c_rzn_scl', 'c_imgn_logo', 'c_hblt', 'c_drcc_empr_id'
	];

    //protected $dateFormat = 'U';

    public $incrementing = false;   
    public $timestamps = false;    
    
	public function unidadorganica() 
	{
	    return $this->hasOne('App\Unidad', 'c_unidad_id', 'c_empresa_id');
	}

	public function perfiles() 
	{
	    return $this->hasMany('App\Perfil', 'c_unidad_id', 'c_empresa_id');
	}

	public function roles() 
	{
	    return $this->hasMany('App\Rol', 'c_unidad_id', 'c_empresa_id');
	}

    public function usuarios()
    {
        return $this->belongsToMany('App\Usuario', 'SEGURIDAD.TB_SG_UNIDAD_USUARIO', 'c_unidad_id', 'c_usuario_id')->using('App\UnidadUsuario');
    } 	
}
