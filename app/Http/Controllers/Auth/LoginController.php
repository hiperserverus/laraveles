<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';//Aca iria el index del veltrix

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function username()
    {
        return 'C_CORREO';
    }
    public function login()
    {
	    $userusuario = Input::get('email');
	    $password = Input::get('password');

		if (Auth::attempt(['c_nombre_usuario' => $userusuario, 'c_clave_usuario' => $password]))
        {
            return Redirect::to('home');
        }
        return Redirect::to('/');
	}
}
