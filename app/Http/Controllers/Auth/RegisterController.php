<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'codigo' => ['required','string','max:10'],
            'nombre' => ['required','string','max:50'],
            'apellido' => ['required','string','max:50'],
            'clave' => ['required','string','max:70'],
            'correo' => ['required','string','max:60'],
            // 'name' => ['required', 'string', 'max:255'],
            // 'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            // 'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
   
    protected function create(array $data)
    {

        

        $usuario = [
            'c_usuario_id' => $data['codigo'],
            'c_perfil_id' => 'PF0000002',
            'c_tipo_usuario_id' => 'PA00000009',
            'c_codigo_usuario' => $data['codigo'],
            'c_nombre_usuario' => $data['nombre'],
            'c_apellido_usuario' => $data['apellido'],
            'c_clave_usuario' => $data['clave'],
            'c_usuario_bloqueado' => 'N',
            'c_cod_usuario' => '0',
            'c_usuario_activo' => 'S',
            'c_auditoria_id' => '0',
            'c_correo' => $data['correo'],
        ];

        $unidad = [
            'c_unidad_id' => 'XX00000018',
            'c_unidad_padre_id' => 'XX00000017',
            'c_tipo_unidad_id' => 'PA00000002',
            'c_cod_unidad' => '5',
            'c_nom_unidad' => $data['empresa'],
            'c_unidad_activa' => 'S',
            'c_auditoria_id' => '0',
        ];

        $empresa = [
            'c_empresa_id' => $unidad['c_unidad_id'],
            'n_ruc' => '20338345986',
            'c_nomb_cmrcl' => $data['empresa'],
            'c_rzn_scl' => $data['empresa'],
            'c_imgn_logo' => '/img/empresa.jpg',
            'c_hblt' => 'S',
            'c_drcc_empr_id' => '0000000003',
        ];

        $unidadusuario = [
            'c_unidad_usuario_id' => 'XX00000009',
            'c_unidad_id' => $unidad['c_unidad_id'],
            'c_usuario_id' => $data['codigo'],
            'c_asignado' => 'S',
            'c_unidad_usuario_activo' => 'S',
            'c_auditoria_id' => '0',
        ];

        DB::beginTransaction();
        try{
            Unidad::create($unidad);
            $n = User::create($usuario);
            Empresa::create($empresa);
            UnidadUsuario::create($unidadusuario);
            DB::commit();
            return $n;
        }catch(\Exception $e){
            DB::rollback();
            throw $e;
        }
        // return User::create($usuario);

    }
}
