<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Persona;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('index');
    }

    public function showSettings() {

      return view('settings');
    }

    public function show() {

      $personas = new Persona();

    $personas = $personas->getAll(10);

  // dd($personas);
  

    return view('personas')->with(['personas'=>$personas]); 

   // return view('index',['personas' => $personas]);

    //return view('index', compact('personas'));


    }


}
