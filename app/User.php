<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'SEGURIDAD.TB_SG_USUARIO';

	protected $fillable = [
		'c_usuario_id', 'c_perfil_id','c_tipo_usuario_id', 'c_codigo_usuario', 'c_nombre_usuario', 'c_apellido_usuario',
		'c_clave_usuario', 'f_asignacion_clave', 'c_usuario_bloqueado', 'c_cod_usuario', 'c_usuario_activo', 'c_auditoria_id','c_correo'
    ];
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
  
    protected $primaryKey = 'c_usuario_id';
    protected $keyType = 'string';

    //protected $dateFormat = 'U';

    public $incrementing = false;   
    public $timestamps = false;   
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    // protected $casts = [
    //     'F_ASIGNACION_CLAVE' => 'datetime',
    // ];
}
