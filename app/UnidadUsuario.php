<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class UnidadUsuario extends Pivot
{
    protected $table="SEGURIDAD.TB_SG_UNIDAD_USUARIO";
    protected $primaryKey = 'c_unidad_usuario_id';
    protected $keyType = 'string';
	protected $fillable = [
		'c_unidad_usuario_id', 'c_unidad_id', 'c_usuario_id', 'c_asignado', 'c_unidad_usuario_activo', 'c_auditoria_id'
	];

    //protected $dateFormat = 'U';

    public $incrementing = false;   
    public $timestamps = false;
}
