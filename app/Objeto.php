<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Objeto extends Model
{
    protected $table="BA.TB_MS_OBJETO";    
    protected $primaryKey = 'c_objeto_id';
    protected $keyType = 'string';
	protected $fillable = [
		'c_objeto_id', 'c_tipo_objeto', 'c_objeto_padre_id', 'c_codigo_objeto', 'c_orden_objeto', 'c_nombre_objeto', 'c_desc_objeto', 'c_version_objeto', 'c_imagen_objeto', 'c_objeto_activo', 'c_auditoria_id'
	];

    //protected $dateFormat = 'U';

    public $incrementing = false;   
    public $timestamps = false;    
        
    public function roles()
    {
        return $this->belongsToMany('App\Rol', 'SEGURIDAD.TB_SG_ROL_OBJETO', 'c_objeto_id', 'c_rol_id')->using('App\RolObjeto');
    }   

    public function usuarios()
    {
        return $this->belongsToMany('App\Usuario', 'SEGURIDAD.TB_SG_USUARIO_OBJETO', 'c_objeto_id', 'c_usuario_id')->using('App\UsuarioObjeto');
    } 
}
