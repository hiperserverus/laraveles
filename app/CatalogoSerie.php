<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CatalogoSerie extends Model
{
    protected $table="BA.TB_MS_CATALOGO_SERIE";    
    protected $primaryKey = 'c_serie_id';
    protected $keyType = 'string';
	protected $fillable = [
		'c_serie_id', 'c_cata_id', 'c_codigo_serie', 'c_desc_serie', 'n_correl_serie', 'c_cata_serie_activa'
	];

    //protected $dateFormat = 'U';

    public $incrementing = false;   
    public $timestamps = false;

    public function catalogo()
    {
        return $this->hasOne('App\Catalogo', 'c_cata_id', 'c_cata_id');
    }    
}
