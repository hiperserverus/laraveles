<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;


class Persona extends Model
{
    protected $table="ADMINDB.LP_PERS";    
    protected $primaryKey = 'CODPER';
    protected $keyType = 'string';
	protected $fillable = [
		'CODCIA', 'CODPER', 'PATPER', 'MATPER', 'NOMPER', 'TPODOC',
        'NRODOC', 'FCHNAC', 'COPANA', 'LUGNAC', 'COPANC', 'SEXO',
        'ESTCIVIL', 'GRPSAN', 'FACTSAN', 'ESTATURA', 'PESO', 'OJOS',
        'CABELLO', 'FLGSRVM', 'AASRVM', 'MMSVRM', 'CODINST', 'CODGRD',
        'CODGRADO', 'NRORUC', 'NROBREV', 'CATBREV', 'CLSBREV', 'FLGEST',
        'FCHING', 'CESSALUD', 'CUSSP', 'CODAUX', 'FLGTRANS', 'COPAEM',
        'FOTO', 'CREA', 'MODIFICA', 'ELIMINA', 'CATEGORIA'
	];

    //protected $dateFormat = 'U';

    public $incrementing = false;   
    public $timestamps = false;

    public function getAll($limit)
{
    $this->persons = Persona::paginate($limit); //Use paginate here.
     return $this->persons;
}
}
