<?php

namespace App;

use App\Empresa;
use App\UnidadUsuario;
use App\Parametro;
use App\Perfil;
use Illuminate\Database\Eloquent\Model;

class Unidad extends Model
{
    //
    protected $table="BASE.TB_MG_UNIDAD";    
    protected $primaryKey = 'c_unidad_id';
    protected $keyType = 'string';
	protected $fillable = [
		'c_unidad_id', 'c_unidad_padre_id', 'c_tipo_unidad_id', 'c_cod_unidad', 'c_nom_unidad', 'c_unidad_activa', 'c_auditoria_id'
	];

    //protected $dateFormat = 'U';
    public $incrementing = false;   
    public $timestamps = false; 

	public function empresa() 
	{
	    return $this->hasOne('App\Empresa', 'c_empresa_id', 'c_unidad_id');
	} 

	public function unidadnegocio() 
	{
	    return $this->hasOne('App\UnidadNegocio', 'c_empresa_id', 'c_division_id');
	} 

	public function grupoempresarial() 
	{
	    return $this->hasOne('App\GrupoEmpresarial', 'c_empresa_id', 'c_holding_id');
	} 	

    public function usuarios()
    {
        return $this->belongsToMany('App\Usuario', 'SEGURIDAD.TB_SG_UNIDAD_USUARIO', 'c_unidad_id', 'c_usuario_id')->using('App\UnidadUsuario');
    } 
}
