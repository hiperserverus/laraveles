<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class PerfilRol extends Pivot
{
    protected $table="SEGURIDAD.TB_SG_PERFIL_ROL";
    protected $primaryKey = 'c_perfil_rol_id';
    protected $keyType = 'string';
	protected $fillable = [
		'c_perfil_rol_id', 'c_perfil_id', 'c_rol_id', 'c_perfil_rol_activo', 'c_auditoria_id'
	];

    //protected $dateFormat = 'U';

    public $incrementing = false;   
    public $timestamps = false;
}
