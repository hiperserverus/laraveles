<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class TipoUsuario extends Parametro
{
    //
    
    protected static function boot()
    {
	    parent::boot();

		static::addGlobalScope('tipousario', function (Builder $builder) {
            $builder->where('c_grupo_parametro_id', '=', 'GP00000003');
        });	    
	}
    

    public function usuarios()
    {
        return $this->hasMany('App\Usuario', 'c_tipo_usuario_id', 'c_parametro_id');
    }

}
