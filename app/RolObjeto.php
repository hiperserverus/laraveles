<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class RolObjeto extends Pivot
{
    protected $table="SEGURIDAD.TB_SG_ROL_OBJETO";
    protected $primaryKey = 'c_rol_objeto_id';
    protected $keyType = 'string';
	protected $fillable = [
		'c_rol_objeto_id', 'c_rol_id', 'c_objeto_id', 'c_ind_visible', 'c_ind_habilitado', 'c_ind_protegido', 'c_rol_objeto_activo', 
		'c_auditoria_id' 
	];

    //protected $dateFormat = 'U';

    public $incrementing = false;   
    public $timestamps = false;
}
