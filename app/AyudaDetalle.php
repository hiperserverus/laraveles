<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AyudaDetalle extends Model
{
    protected $table="BA.TB_MS_AYUDA_DTLLE";    
    protected $primaryKey = 'c_ayuda_detalle_id';
    protected $keyType = 'string';
	protected $fillable = [
		'c_ayuda_detalle_id', 'c_ayuda_cabecera_id', 'c_nomb_campo', 'n_orden', 'c_alias_campo', 'c_titulo_campo', 'c_atributo_campo',
		'c_atributo_titulo'
	];

    //protected $dateFormat = 'U';

    public $incrementing = false;   
    public $timestamps = false;    

    public function ayuda()
    {
        return $this->hasOne('App\Ayuda', 'c_ayuda_cabecera_id', 'c_ayuda_cabecera_id');
    }       
}
