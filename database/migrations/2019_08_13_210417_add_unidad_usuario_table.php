<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUnidadUsuarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('TB_SG_UNIDAD_USUARIO', function (Blueprint $table) {
            $table->string('C_UNIDAD_USUARIO_ID', 10)->primary();
            $table->string('C_UNIDAD_ID')->nullable();
            $table->foreign('C_UNIDAD_ID')->references('C_UNIDAD_ID')->on('TB_MG_UNIDAD')->onDelete('cascade');
            $table->string('C_USUARIO_ID')->nullable();
            $table->foreign('C_USUARIO_ID')->references('C_USUARIO_ID')->on('TB_SG_USUARIO')->onDelete('cascade');
            $table->char('C_ASIGNADO',1);
            $table->string('C_AUDTIORIA_ID', 10);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('TB_SG_UNIDAD_USUARIO');
    }
}
