<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUnidadTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('TB_MG_UNIDAD', function (Blueprint $table) {
            $table->string('C_UNIDAD_ID',10)->primary();
            $table->string('C_UNIDAD_PADRE_ID',10);
            $table->string('C_TIPO_UNIDAD_ID')->nullable();
            $table->foreign('C_TIPO_UNIDAD_ID')->references('C_PARAMETRO_ID')->on('TB_MS_PARAMETRO')->onDelete('cascade');
            $table->string('C_COD_UNIDAD',10);
            $table->string('C_NOM_UNIDAD',50);
            $table->char('C_UNIDAD_ACTIVA',1);
            $table->string('C_AUDITORIA_ID',10);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('TB_MG_UNIDAD');
    }
}
