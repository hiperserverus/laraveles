<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRolObjetoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('TB_SG_ROL_OBJETO', function (Blueprint $table) {
            $table->string('C_ROL_OBJETO_ID', 10)->primary();
            $table->string('C_ROL_ID')->nullable();
            $table->foreign('C_ROL_ID')->references('C_ROL_ID')->on('TB_SG_ROL')->onDelete('cascade');
            $table->string('C_OBJETO_ID')->nullable();
            $table->foreign('C_OBJETO_ID')->references('C_OBJETO_ID')->on('TB_MS_OBJETO')->onDelete('cascade');
            $table->enum('C_IND_VISIBLE',['S','N'])->default('S');
            $table->enum('C_IND_HABILITADO',['S','N'])->default('S');
            $table->enum('C_IND_PROTEGIDO',['S','N'])->default('S');
            $table->enum('C_ROL_OBJETO_ACTIVO',['S','N'])->default('S');
            $table->string('C_AUDITORIA_ID', 10);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('TB_SG_ROL_OBJETO');
    }
}
