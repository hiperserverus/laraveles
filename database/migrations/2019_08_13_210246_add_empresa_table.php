<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEmpresaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('TB_MG_EMPRESA', function (Blueprint $table) {
            $table->string('C_EMPRESA_ID',10)->primary();
            $table->string('C_UNIDAD_ID')->nullable();
            $table->foreign('C_UNIDAD_ID')->references('C_UNIDAD_ID')->on('TB_MG_UNIDAD')->onDelete('cascade');
            $table->string('N_RUC',11);
            $table->string('C_NOMB_CMRCL',200);
            $table->string('C_RZN_SCL',200);
            $table->string('C_IMGN_LOGO',50);
            $table->char('C_HBLT',1);
            $table->string('C_DRCC_EMPR_ID',10);
            $table->string('C_DOMINIO',50);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('TB_MG_EMPRESA');
    }
}
