<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddObjetoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('TB_MS_OBJETO', function (Blueprint $table) {
            $table->string('C_OBJETO_ID',10)->primary();
            $table->string('C_TIPO_OBJETO',10)->nullable();
            $table->foreign('C_TIPO_OBJETO')->references('C_PARAMETRO_ID')->on('TB_MS_PARAMETRO')->onDelete('cascade');
            $table->string('C_OBJETO_PADRE_ID', 10);
            $table->string('C_CODIGO_OBJETO', 20);
            $table->integer('C_ORDEN_OBJETO');
            $table->string('C_DESC_OBJETO', 50);
            $table->string('C_VERSION_OBJETO', 10);
            $table->string('C_IMAGEN_OBJETO', 50);
            $table->enum('C_OBJETO_ACTIVO',['S','N'])->default('S');
            $table->string('C_AUDITORIA_ID', 10);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('TB_MS_OBJETO');
    }
}
