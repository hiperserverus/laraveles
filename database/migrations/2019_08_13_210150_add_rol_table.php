<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRolTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('TB_SG_ROL', function (Blueprint $table) {
            $table->string('C_ROL_ID', 10)->primary();
            $table->string('C_UNIDAD_ID')->nullable();
            $table->foreign('C_UNIDAD_ID')->references('C_UNIDAD_ID')->on('TB_MG_UNIDAD')->onDelete('cascade');
            $table->string('C_CODIGO_ROL', 20);
            $table->string('C_DESC_ROL', 50);
            $table->enum('C_ROL_HEREDABLE',['S','N'])->default('S');
            $table->enum('C_ROL_ACTIVO',['S','N'])->default('S');
            $table->string('C_AUDITORIA_ID', 10);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('TB_SG_ROL');
    }
}
