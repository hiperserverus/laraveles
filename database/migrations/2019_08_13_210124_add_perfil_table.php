<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPerfilTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('TB_SG_PERFIL', function (Blueprint $table) {
            $table->string('C_PERFIL_ID', 10)->primary();
            $table->string('C_UNIDAD_ID')->nullable();
            $table->foreign('C_UNIDAD_ID')->references('C_UNIDAD_ID')->on('TB_MG_UNIDAD')->onDelete('cascade');
            $table->string('C_CODIGO_PERFIL', 5);
            $table->string('C_NOMBRE_PERFIL', 30);
            $table->enum('C_PERFIL_HEREDABLE',['S','N'])->default('S');
            $table->enum('C_PERFIL_ACTIVO',['S','N'])->default('S');
            $table->string('C_AUDITORIA_ID');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('TB_SG_PERFIL');
    }
}
