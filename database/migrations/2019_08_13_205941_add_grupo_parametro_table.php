<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGrupoParametroTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('TB_MS_GRUPO_PARAMETRO', function (Blueprint $table) {
            $table->string('C_GRUPO_PARAMETRO_ID')->primary();
            $table->string('C_CODIGO_GRUPO_PARAMETRO', 10);
            $table->string('C_DES_GRUPO_PARAMETRO', 50);
            $table->enum('C_GRUPO_PARAMETRO_ACTIVO', ['S','N'])->default('S');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('TB_MS_GRUPO_PARAMETRO');
    }
}
