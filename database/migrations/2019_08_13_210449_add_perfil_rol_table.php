<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPerfilRolTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('TB_SG_PERFIL_ROL', function (Blueprint $table) {
            $table->string('C_PERFIL_ROL_ID', 10)->primary();
            $table->string('C_PERFIL_ID', 10)->nullable();
            $table->foreign('C_PERFIL_ID')->references('C_PERFIL_ID')->on('TB_SG_PERFIL')->onDelete('cascade');
            $table->string('C_ROL_ID')->nullable();
            $table->foreign('C_ROL_ID')->references('C_ROL_ID')->on('TB_SG_ROL');
            $table->enum('C_PERFIL_ROL_ACTIVO',['S','N'])->default('S');
            $table->string('C_AUDITORIA_ID', 10);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('TB_SG_PERFIL_ROL');
    }
}
