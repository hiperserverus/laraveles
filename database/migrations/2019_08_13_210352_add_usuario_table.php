<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUsuarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('TB_SG_USUARIO', function (Blueprint $table) {
            $table->string('C_USUARIO_ID', 10)->primary();
            $table->string('C_PERFIL_ID', 10)->nullable();
            $table->foreign('C_PERFIL_ID')->references('C_PERFIL_ID')->on('TB_SG_PERFIL')->onDelete('cascade');
            $table->string('C_TIPO_USUARIO_ID', 10)->nullable();
            $table->foreign('C_TIPO_USUARIO_ID')->references('C_PARAMETRO_ID')->on('TB_MS_PARAMETRO')->onDelete('cascade');
            $table->string('C_CODIGO_USUARIO', 15);
            $table->string('C_NOMBRE_USUARIO', 50);
            $table->string('C_APELLIDO_USUARIO', 50);
            $table->string('C_CLAVE_USUARIO', 70);
            $table->dateTime('F_ASIGNACION_CLAVE');
            $table->char('C_USUARIO_BLOQUEADO', 1);
            $table->string('C_COD_USUARIO');
            $table->char('C_USUARIO_ACTIVO', 1);
            $table->string('C_AUDITORIA_ID');
            $table->string('C_DOMINIO', 50);
            $table->string('C_CORREO', 100);
            $table->string('C_TELEFONO', 20);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('TB_SG_USUARIO');
    }
}
