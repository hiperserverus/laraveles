<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUsuarioRolTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('TB_SG_USUARIO_ROL', function (Blueprint $table) {
            $table->string('C_USUARIO_ROL_ID', 10)->primary();
            $table->string('C_USUARIO_ID')->nullable();
            $table->foreign('C_USUARIO_ID')->references('C_USUARIO_ID')->on('TB_SG_USUARIO')->onDelete('cascade')->onDelete('cascade');
            $table->string('C_ROL_ID')->nullable();
            $table->foreign('C_ROL_ID')->references('C_ROL_ID')->on('TB_SG_ROL')->onDelete('cascade')->onDelete('cascade');
            $table->enum('C_USUARIO_ROL_ACTIVO',['S','N'])->default('S');
            $table->string('C_AUDITORIA_ID', 10);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('TB_SG_USUARIO_ROL');
    }
}
