<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddParametroTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('TB_MS_PARAMETRO', function (Blueprint $table) {
            $table->string('C_PARAMETRO_ID', 10)->primary();
            $table->string('C_GRUPO_PARAMETRO_ID')->nullable();
            $table->foreign('C_GRUPO_PARAMETRO_ID')->references('C_GRUPO_PARAMETRO_ID')->on('TB_MS_GRUPO_PARAMETRO')->onDelete('cascade');
            $table->string('C_CODIGO_PARAMETRO', 10);
            $table->string('C_DESC_PARAMETRO', 100);
            $table->enum('C_PARAMETRO_ACTIVO', ['S','N'])->default('S');
            $table->string('C_AUDITORIA_ID', 10);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('TB_MS_PARAMETRO');
    }
}
