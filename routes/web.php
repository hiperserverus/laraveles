<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => 'web'], function(){
    Route::get('/', function () {
        return view('welcome');
    });
    Route::get('/dashboard', 'HomeController@index')->name('dashboard');
    Route::get('/settings', 'HomeController@showSettings')->name('settings');
    // Route::get('/persons', 'HomeController@show')->name('personas');
    

    //Route::auth();

    //Rutas de logeo

    Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
    Route::post('login', 'Auth\LoginController@login')->name('login.log');
    Route::post('logout', 'Auth\LoginController@logout')->name('logout');

    //Rutas de registro

    Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
    Route::post('register', 'Auth\RegisterController@register');

    Route::get('/home', 'HomeController@index');
});




