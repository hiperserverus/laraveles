@extends('layouts.master-blank')

@section('content')
       <div class="home-btn d-none d-sm-block">
            <a href="index" class="text-dark"><i class="fas fa-home h2"></i></a>
        </div>

        <div class="wrapper-page">
            <div class="card overflow-hidden account-card mx-3">
                <div class="bg-primary p-4 text-white text-center position-relative">
                    <h4 class="font-20 m-b-5">Registrate</h4>
                    <p class="text-white-50 mb-4">Crea tu cuenta.</p>
                    <a href="index" class="logo logo-admin"><img src="{{ URL::asset('assets/images/logo-sm.png') }}" height="24" alt="logo"></a>
                </div>
                <div class="account-card-content">

                {!! Form::open(['route'=>'register.create', 'method'=>'POST']) !!}

                    {!! Form::token() !!}

                        <div class="form-group">
                        {!! Form::label('nombre', 'Nombre', array('class' => 'negrita')) !!}
                        {!! Form::text('nombre',null,['class'=>'form-control', 'placeholder'=>'Nombre', 'required' => 'required']) !!}
                        </div>

                        <div class="form-group">
                        {!! Form::label('apellido', 'Apellido', array('class' => 'negrita')) !!}
                        {!! Form::text('apellido',null,['class'=>'form-control', 'placeholder'=>'Apellido', 'required' => 'required']) !!}
                        </div>

                        <div class="form-group">
                        {!! Form::label('correo', 'Correo', array('class' => 'negrita')) !!}
                        {!! Form::text('correo',null,['class'=>'form-control', 'placeholder'=>'Correo', 'required' => 'required']) !!}
                        </div>

                        <div class="form-group">
                        {!! Form::label('dominio', 'Dominio', array('class' => 'negrita')) !!}
                        {!! Form::text('dominio',null,['class'=>'form-control', 'placeholder'=>'Dominio', 'required' => 'required']) !!}
                        </div>

                        <div class="form-group">
                        {!! Form::label('telefono', 'Telefono', array('class' => 'negrita')) !!}
                        {!! Form::text('telefono',null,['class'=>'form-control', 'placeholder'=>'Telefono', 'required' => 'required']) !!}
                        </div>

                        <div class="form-group row m-t-20">
                            <div class="col-12 text-right">
                         
	                            {!!Form::submit('Registrar', ['class' => 'btn btn-primary '])!!}

                            </div>
                        </div>

                        <div class="form-group m-t-10 mb-0 row">
                            <div class="col-12 m-t-20">
                                <p class="mb-0">By Junior Gonzalez <a href="#" class="text-primary">Terminos de Uso</a></p>
                            </div>
                        </div>

                        {!! Form::close() !!}
                </div>
            </div>

            <div class="m-t-40 text-center">
                <p>Tienes una cuenta ? <a href="pages-login" class="font-500 text-primary"> Ingresar </a> </p>
                <p>© {{date('Y')}} AcistPeru <i class="mdi mdi-heart text-danger"></i> by Junior Gonzalez</p>
            </div>

        </div>
        <!-- end wrapper-pags -->
@endsection

@section('script')
@endsection