@extends('layouts.master')

@section('css')
<!--Chartist Chart CSS -->
<link rel="stylesheet" href="{{ URL::asset('plugins/chartist/css/chartist.min.css') }}">

<link href="{{ URL::asset('plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css') }}" rel="stylesheet">
<link href="{{ URL::asset('plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
<link href="{{ URL::asset('plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('plugins/bootstrap-touchspin/css/jquery.bootstrap-touchspin.min.css') }}" rel="stylesheet" /> 
@endsection

@section('breadcrumb')
<div class="col-sm-6">
     <h4 class="page-title">Configuracion</h4>
     <ol class="breadcrumb">
         <li class="breadcrumb-item active">Configuraciones generales del sistema</li>
     </ol>
</div>
@endsection

@section('content')

  <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-body">
        
                            
        
                                        <!-- Nav tabs -->
                                        <ul class="nav nav-tabs nav-tabs-custom nav-center" role="tablist">
                                            <li class="nav-item">
                                                <a class="nav-link active" data-toggle="tab" href="#home2" role="tab">
                                                    <span class="d-block d-sm-none"><i class="fas fa-home"></i></span>
                                                    <span class="d-none d-sm-block">Usuario</span> 
                                                </a>

                                                

                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" data-toggle="tab" href="#profile2" role="tab">
                                                    <span class="d-block d-sm-none"><i class="far fa-user"></i></span>
                                                    <span class="d-none d-sm-block">Empresa</span> 
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" data-toggle="tab" href="#messages2" role="tab">
                                                    <span class="d-block d-sm-none"><i class="far fa-envelope"></i></span>
                                                    <span class="d-none d-sm-block">Plantillas</span>   
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" data-toggle="tab" href="#settings2" role="tab">
                                                    <span class="d-block d-sm-none"><i class="fas fa-cog"></i></span>
                                                    <span class="d-none d-sm-block">Lista de Usuarios</span>    
                                                </a>
                                            </li>

                                            <li class="nav-item">
                                                <a class="nav-link" data-toggle="tab" href="#settings2" role="tab">
                                                    <span class="d-block d-sm-none"><i class="fas fa-cog"></i></span>
                                                    <span class="d-none d-sm-block">Horario</span>    
                                                </a>
                                            </li>

                                            <li class="nav-item">
                                                <a class="nav-link" data-toggle="tab" href="#settings2" role="tab">
                                                    <span class="d-block d-sm-none"><i class="fas fa-cog"></i></span>
                                                    <span class="d-none d-sm-block">Servidor de Correos</span>    
                                                </a>
                                            </li>

                                            <li class="nav-item">
                                                <a class="nav-link" data-toggle="tab" href="#settings2" role="tab">
                                                    <span class="d-block d-sm-none"><i class="fas fa-cog"></i></span>
                                                    <span class="d-none d-sm-block">Servidor de Sunat</span>    
                                                </a>
                                            </li>

                                             <li class="nav-item">
                                                <a class="nav-link" data-toggle="tab" href="#settings2" role="tab">
                                                    <span class="d-block d-sm-none"><i class="fas fa-cog"></i></span>
                                                    <span class="d-none d-sm-block">Correos de Notificaciones</span>    
                                                </a>
                                            </li>
                                        </ul>
        
                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                            <div class="tab-pane active p-3" id="home2" role="tabpanel">

                               <div class="col-lg-4 ">
                                <div class="card">
                                    <div class="card-header">
                                       <strong >Nombre Usuario </strong>: Manuel Vasquez
                                    </div>
                                    <div class="card-body">
                                        <blockquote class="card-blockquote mb-0">
                                        <p> <img src="assets/images/users/user-2.jpg" alt="" class="thumb-md rounded-circle mr-2"><strong>20100793653-MVASQUEZ</strong></p>
                                             
                                            <footer class="blockquote-footer font-12 negrita">
                                               <strong>ROL DE USUARIO: </strong> <cite title="Source Title " class="badge badge-pill badge-danger noti-icon-badge">ADMINISTRADOR</cite>
                                            </footer>
                                        </blockquote>
                                    </div>
                                </div>
                            </div>
                                      
                                               <div class="card-body">
        
                                        <h4 class="mt-0 header-title">Personalizar usuario:</h4>

                                    
        
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-sm-2 col-form-label">Nombres</label>
                                            <div class="col-sm-10">
                                                <input class="form-control" type="text" placeholder="Nombres" id="example-text-input">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-sm-2 col-form-label">Apellidos</label>
                                            <div class="col-sm-10">
                                                <input class="form-control" type="text" placeholder="Apellidos" id="example-text-input">
                                            </div>
                                        </div>
                                       
                                        <div class="form-group row">
                                            <label for="example-email-input" class="col-sm-2 col-form-label">Email</label>
                                            <div class="col-sm-10">
                                                <input class="form-control" type="email" placeholder="correoelectronico@ejemplo.com" id="example-email-input">
                                            </div>
                                        </div>
                                      
                                        <div class="form-group row">
                                            <label for="example-password-input" class="col-sm-2 col-form-label">Contraseña</label>
                                            <div class="col-sm-10">
                                                <input class="form-control" type="password" placeholder="Contraseña actual" id="example-password-input">
                                                
                                            </div>
                                       </div>

                                         <div class="form-group row">
                                            <label for="example-password-input" class="col-sm-2 col-form-label">Nueva Contraseña</label>
                                            <div class="col-sm-10">
                                                <input class="form-control" type="password" placeholder="Nueva Contraseña" id="example-password-input">
                                                
                                            </div>
                                       </div>

                                         <div class="form-group row">
                                            <label for="example-password-input" class="col-sm-2 col-form-label">Repetir Contraseña</label>
                                            <div class="col-sm-10">
                                                <input class="form-control" type="password" placeholder="Nueva contraseña" id="example-password-input">
                                                
                                            </div>
                                       </div>
                                       

                                    <input data-repeater-create type="button" class="btn btn-success inner" value="Actualizar"/>
                                </div>
                                            </div>
                                            
                                            <div class="tab-pane p-3" id="profile2" role="tabpanel">
                                             {{--  --}}
                                     <div class="row">
                                        <div class="col-6">
                                
                                      
                                               <div class="card-body">
        
                                        <h4 class="mt-0 header-title">Información Empresa:</h4>

                                
        
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-sm-3 col-form-label">Razon social:</label>
                                            <div class="col-sm-9">
                                                <input disabled class="form-control" type="text" placeholder="Nombres" id="example-text-input">
                                            </div>
                                        </div>

                                            </div>
                                        </div>

       <div class="col-6">
                                
                                      
                                               <div class="card-body">
        
                                        <h4 class="mt-0 header-title">Configuración del Certificado:</h4>

                                
        
                                            <div class="form-group row">
                                            <label for="example-password-input" class="col-sm-3 col-form-label">Clave certificado:</label>
                                            <div class="col-sm-9">
                                                <input class="form-control" type="password" placeholder="clave del certificado" id="example-password-input">
                                                
                                            </div>
                                       </div>

                                            </div>
                                        </div>

                                        </div>

                                                                      <div class="row">
                                             <div class="col-6">
                                    <div class="card-body">
        
                                        <h4 class="mt-0 header-title">Logo empresarial</h4>
                                    
        
                                        <form action="#">
                                            <div class="form-group">
                                                <label>Seleccionar archivo</label>
                                                <input  type="file" class="filestyle" data-buttonname="btn-secondary">
                                            </div>
        
                                          
        
                                        </form>
                                    </div>
                                </div>

                 <div class="col-6">
                                    <div class="card-body">
        
                                        <h4 class="mt-0 header-title">Subir certificado:</h4>
                                    
        
                                        <form action="#">
                                            <div class="form-group">
                                                <label>Seleccionar archivo</label>
                                                <input  type="file" class="filestyle" data-buttonname="btn-secondary">
                                            </div>
        
                                          
        
                                        </form>
                                    </div>
                                </div>

                                        </div>

                                                             <div class="row">
                                        <div class="col-6">
                                
                                      
                                               <div class="card-body">
        
                                        <h4 class="mt-0 header-title">Configuración de la clave Sol:</h4>

                                
        
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-sm-3 col-form-label">Ruc:</label>
                                            <div class="col-sm-9">
                                                <input disabled class="form-control" type="text" placeholder="Ruc" id="example-text-input">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-sm-3 col-form-label">Unidad Sol:</label>
                                            <div class="col-sm-9">
                                                <input class="form-control" type="text" placeholder="Unidad sol" id="example-text-input">
                                            </div>
                                        </div>

                                          <div class="form-group row">
                                            <label for="example-password-input" class="col-sm-3 col-form-label">Clave sol:</label>
                                            <div class="col-sm-9">
                                                <input class="form-control" type="password" placeholder="Clave sol" id="example-password-input">
                                                
                                            </div>

                                            </div>
                                        </div>

                                        </div>

                                        </div>
                                             {{--  --}}
                                            </div>
                                            <div class="tab-pane p-3" id="messages2" role="tabpanel">
                                               3
                                            </div>
                                            <div class="tab-pane p-3" id="settings2" role="tabpanel">
                                             4
                                            </div>
                                        </div>
        
                                    </div>
                                </div>
                            </div>
        
                        </div>
				

@endsection

@section('script')
<!--Chartist Chart-->
<script src="{{ URL::asset('plugins/chartist/js/chartist.min.js') }}"></script>
<script src="{{ URL::asset('plugins/chartist/js/chartist-plugin-tooltip.min.js') }}"></script>
<!-- peity JS -->
<script src="{{ URL::asset('plugins/peity-chart/jquery.peity.min.js') }}"></script>
<script src="{{ URL::asset('assets/pages/dashboard.js') }}"></script>

<!-- Echart Chart -->
<script src="{{ URL::asset('plugins/e-charts/echarts.min.js') }}"></script>
<!-- Echart int -->
<script src="{{ URL::asset('assets/pages/echart.int.js') }}"></script>  

<!-- Plugins js -->
<script src="{{ URL::asset('plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js') }}"></script>
<script src="{{ URL::asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
<script src="{{ URL::asset('plugins/select2/js/select2.min.js') }}"></script>
<script src="{{ URL::asset('plugins/bootstrap-maxlength/bootstrap-maxlength.min.js') }}"></script>
<script src="{{ URL::asset('plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js') }}"></script>
<script src="{{ URL::asset('plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js') }}"></script>
<!-- Plugins Init js -->
<script src="{{ URL::asset('assets/pages/form-advanced.js') }}"></script>
@endsection