@extends('layouts.master-blank')

@section('content')
        <div class="home-btn d-none d-sm-block">
            <a href="index" class="text-dark"><i class="fas fa-home h2"></i></a>
        </div>

        <div class="wrapper-page">
            <div class="card overflow-hidden account-card mx-3">
                <div class="bg-primary p-4 text-white text-center position-relative">
                    <h4 class="font-20 m-b-5">Bienvenido</h4>
                    <p class="text-white-50 mb-4">Ingreso del sistema.</p>
                    <a href="index" class="logo logo-admin"><img src="{{ URL::asset('assets/images/logo-sm.png') }}" height="24" alt="logo"></a>
                </div>
                <div class="account-card-content">
                    {!! Form::open([ 'route' => 'login.log', 'method' => 'POST']) !!}
                     
                        {!! Form::token() !!}

                        <div class="form-group">
                        {!! Form::label('indentificador', 'Ruc', array('class' => 'negrita')) !!}
                        {!! Form::text('identificador',null,['class'=>'form-control', 'placeholder'=>'Ruc', 'required' => 'required']) !!}
                        </div>

                        <div class="form-group">
                        {!! Form::label('email', 'Usuario', array('class' => 'negrita')) !!}
                        {!! Form::email('email',null,['class'=>'form-control', 'placeholder'=>'Usuario', 'required' => 'required']) !!}
                        </div>

                        <div class="form-group">
                            <label for="userpassword">Password</label>
                            <input type="password" class="form-control" id="userpassword" placeholder="Enter password">
                        </div>

                        <div class="form-group row m-t-20">
                        <div class="col-sm-6">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="customControlInline">
                                    <label class="custom-control-label" for="customControlInline">Remember me</label>
                                </div>
                            </div>
                            <div class="col-sm-6 text-right">
                            {!!Form::submit('Registrar', ['class' => 'btn btn-primary '])!!}
                            </div>
                        </div>

                        <div class="form-group m-t-10 mb-0 row">
                            <div class="col-12 m-t-20">
                                <a href="pages-recoverpw"><i class="mdi mdi-lock"></i> Olvidaste tu contraseña?</a>
                            </div>
                        </div>

                         {!! Form::close() !!}
                </div>
            </div>

            <div class="m-t-40 text-center">
                <p>No tienes cuenta ? <a href="pages-register" class="font-500 text-primary"> Registrate ahora </a> </p>
                <p>© {{date('Y')}} AcistPeru <i class="mdi mdi-heart text-danger"></i> by Junior Gonzalez</p>
            </div>

        </div>
        <!-- end wrapper-page -->
@endsection

@section('script')
@endsection