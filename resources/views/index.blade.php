@extends('layouts.master')

@section('css')
<!--Chartist Chart CSS -->
<link rel="stylesheet" href="{{ URL::asset('plugins/chartist/css/chartist.min.css') }}">
@endsection

@section('breadcrumb')
<div class="col-sm-6">
     <h4 class="page-title">Dashboard</h4>
     <ol class="breadcrumb">
         <li class="breadcrumb-item active">Cuadro estadístico
Estadísticas de ventas y compras anuales</li>
     </ol>
</div>
@endsection

@section('content')
						<div class="row">
                            <div class="col-md-12">
                                <div class="card mini-stat bg-primary text-white">
                                    <div class="card-body">
                                        <div class="mb-4">
                                            <div class="float-left mini-stat-img mr-4">
                                                <img src="{{ URL::asset('assets/images/services-icon/01.png') }}" alt="" >
                                            </div>
                                            <h5 class="font-16 text-uppercase mt-0 text-white-50">Total de Ventas:</h5>
                                            <h4 class="font-500">1000000,685 /S.<i class="mdi mdi-arrow-up text-success ml-2"></i></h4>
                                            <div class="mini-stat-label bg-success">
                                                <p class="mb-0">+ 99%</p>
                                            </div>
                                        </div>
                                        <div class="pt-2">
                                            <div class="float-right">
                                                <a href="#" class="text-white-50"><i class="mdi mdi-arrow-right h5"></i></a>
                                            </div>
                                            <p class="text-white-50 mb-0">0 Documentos Recibidos</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                          
                        </div>
                        <!-- end row -->

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="mt-0 header-title mb-4">Line Chart</h4>
                                        <div id="line-chart" class="e-chart"></div>
                                    </div>
                                </div>
                            </div>
                          
                        </div>

                        <div class="row">
                        
                        <div class="col-lg-4">
                                <div class="card">
                                    <div class="card-header text-center  font-18">
                                    FACTURAS
                                    </div>
                                    <div class="card-body">
                                        <blockquote class="card-blockquote mb-0">
                                          <p class="text-center font-18">0</p>
                                            
                                        </blockquote>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <div class="card">
                                    <div class="card-header text-center  font-18">
                                    BOLETAS DE VENTA
                                    </div>
                                    <div class="card-body">
                                        <blockquote class="card-blockquote mb-0">
                                          <p class="text-center font-18">0</p>
                                            
                                        </blockquote>
                                    </div>
                                </div>
                            </div>

                          <div class="col-lg-4">
                                <div class="card">
                                    <div class="card-header text-center  font-18">
                                    NOTAS DE CREDITO
                                    </div>
                                    <div class="card-body">
                                        <blockquote class="card-blockquote mb-0">
                                          <p class="text-center font-18">0</p>
                                            
                                        </blockquote>
                                    </div>
                                </div>
                            </div>

                        
                        
                        </div>

                        </div>

                                  <div class="row">
                        
                        <div class="col-lg-4">
                                <div class="card">
                                    <div class="card-header text-center  font-18">
                                    NOTAS DE DEBITO
                                    </div>
                                    <div class="card-body">
                                        <blockquote class="card-blockquote mb-0">
                                          <p class="text-center font-18">0</p>
                                            
                                        </blockquote>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <div class="card">
                                    <div class="card-header text-center  font-18">
                                    COMPROBANTES DE RETENCION
                                    </div>
                                    <div class="card-body">
                                        <blockquote class="card-blockquote mb-0">
                                          <p class="text-center font-18">0</p>
                                            
                                        </blockquote>
                                    </div>
                                </div>
                            </div>

                          <div class="col-lg-4">
                                <div class="card">
                                    <div class="card-header text-center  font-18">
                                   COMPROBANTES DE PERCEPCION
                                    </div>
                                    <div class="card-body">
                                        <blockquote class="card-blockquote mb-0">
                                          <p class="text-center font-18">0</p>
                                            
                                        </blockquote>
                                    </div>
                                </div>
                            </div>

                        
                        
                        </div>

                        </div>

@endsection

@section('script')
<!--Chartist Chart-->
<script src="{{ URL::asset('plugins/chartist/js/chartist.min.js') }}"></script>
<script src="{{ URL::asset('plugins/chartist/js/chartist-plugin-tooltip.min.js') }}"></script>
<!-- peity JS -->
<script src="{{ URL::asset('plugins/peity-chart/jquery.peity.min.js') }}"></script>
<script src="{{ URL::asset('assets/pages/dashboard.js') }}"></script>

<!-- Echart Chart -->
<script src="{{ URL::asset('plugins/e-charts/echarts.min.js') }}"></script>
<!-- Echart int -->
<script src="{{ URL::asset('assets/pages/echart.int.js') }}"></script>  
@endsection