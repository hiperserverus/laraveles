      <!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">
                <div class="slimscroll-menu" id="remove-scroll">

                    <!--- Sidemenu -->
                    <div id="sidebar-menu">
                        <!-- Left Menu Start -->
                        <ul class="metismenu" id="side-menu">
                            <li class="menu-title">Inicio</li>
                            <li>
                                <a href="index" class="waves-effect">
                                    <i class="ti-home"></i><span class="badge badge-primary  float-right"></span> <span> Dashboard </span>
                                </a>
                            </li>
                            

                           

                            <li class="menu-title">Menu</li>

                            <li>
                                <a href="javascript:void(0);" class="waves-effect"><i class="ti-package"></i> <span> Ventas<span class="float-right menu-arrow"><i class="mdi mdi-chevron-right"></i></span> </span> </a>
                                <ul class="submenu">
                                    <li><a href="ui-alerts">Facturas</a></li>
                                    <li><a href="ui-buttons">Boletas de venta</a></li>
                                    <li><a href="ui-cards">Notas de credito</a></li>
                                    <li><a href="ui-carousel">Notas de debito</a></li>
      
                                </ul>
                            </li>

            
                     

                            <li>
                                <a href="javascript:void(0);" class="waves-effect"><i class="ti-package"></i> <span> Otros comprobantes<span class="float-right menu-arrow"><i class="mdi mdi-chevron-right"></i></span> </span> </a>
                                <ul class="submenu">
                                    <li><a href="ui-alerts">Comprobante Retencion</a></li>
                                    <li><a href="ui-buttons">Comprobante Percepcion</a></li>
                                    
      
                                </ul>
                            </li>

                    

                            <li>
                                <a href="javascript:void(0);" class="waves-effect"><i class="ti-package"></i> <span> Guias<span class="float-right menu-arrow"><i class="mdi mdi-chevron-right"></i></span> </span> </a>
                                <ul class="submenu">
                                    <li><a href="ui-alerts">Guia Remision</a></li>
                                    <li><a href="ui-buttons">Guia Transportista</a></li>
              
      
                                </ul>
                            </li>

                  

                            <li>
                                <a href="javascript:void(0);" class="waves-effect"><i class="ti-package"></i> <span> Resumenes<span class="float-right menu-arrow"><i class="mdi mdi-chevron-right"></i></span> </span> </a>
                                <ul class="submenu">
                                    <li><a href="ui-alerts">Resumkens de Boletas</a></li>
                                    <li><a href="ui-buttons">Resumens de Anuladas</a></li>
                                    <li><a href="ui-cards">Reversiones de comprobantes de Retencion y Percepcion</a></li>
                                   
      
                                </ul>
                            </li>

                     

                            <li>
                                <a href="javascript:void(0);" class="waves-effect"><i class="ti-package"></i> <span> Cuadre operaciones<span class="float-right menu-arrow"><i class="mdi mdi-chevron-right"></i></span> </span> </a>
                                <ul class="submenu">
                                    <li><a href="ui-alerts">Puntos de venta</a></li>
                             
      
                                </ul>
                            </li>

                   

                        </ul>

                    </div>
                    <!-- Sidebar -->
                    <div class="clearfix"></div>

                </div>
                <!-- Sidebar -left -->

            </div>
            <!-- Left Sidebar End -->
